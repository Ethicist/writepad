var textfield = document.getElementsByClassName('textfield')[0];
var header = document.getElementsByClassName('header')[0];

function key_check(e) {
  var action = window.event ? event : e
  if (action.ctrlKey) {
    switch (action.keyCode) {
      case 83: // Ctrl+C
        save_file();
        action.preventDefault();
        break;
      case 46: // Ctrl+Delete
        clear_page();
        break;
      default:
        break;
    }
  }
  if (action.keyCode == 27) {
    textfield.focus();
    return false;
  }
}

document.addEventListener('keydown', function (e) {
    key_check();
}, false);

// document.onkeydown = key_check;

// ENTER or TAB key pressed on header
header.onkeyup = function (e) {
  var action = window.event ? event : e
  if (action.keyCode == 13) {
    textfield.focus();
  }
  save();
}

function save() {
  if (localStorage) {
    localStorage.setItem('header', header.value);
    localStorage.setItem('textfield', textfield.value);
  }
}


function load() {
  if (localStorage) {
    header.value = localStorage.getItem('header');
    textfield.value = localStorage.getItem('textfield');
  }
}

window.onstorage = function (e) {
  load();
}

load();

function save_file() {
  var text = textfield.value;
  var blob = new Blob([text], {
    type: 'text/plain'
  });
  var time = new Date();
  var hh = (time.getHours() < 10) ? '0' + time.getHours() : time.getHours();
  var mm = (time.getMinutes() < 10) ? '0' + time.getMinutes() : time.getMinutes();
  var ss = (time.getSeconds() < 10) ? '0' + time.getSeconds() : time.getSeconds();
  var file;
  if (header.value == undefined || header.value == '') {
    file = 'writepad' + '_' + hh + mm + ss + '.txt';
  } else {
    file = header.value + '_' + hh + mm + ss + '.txt';
  }
  try {
    var link = document.createElement('a');
    link.download = file.replace(/ /g, '_');
    link.innerHTML = 'Download File';
    link.href = window.URL.createObjectURL(blob);
    link.style.display = 'none';
    document.body.appendChild(link);
    link.click();
  } catch (exception) {
    alert('К сожалению Ваш браузер не может сохранить файл. Тем не менее, весь введённый текст будет сохранён в памяти браузера.');
  }
}

function clear_page() {
  header.value = '';
  textfield.value = '';
  save();
  // window.location.reload();
}
